package br.com.builderstest.builders.controller;


import br.com.builderstest.builders.calculoIdade.CalculoIdade;
import br.com.builderstest.builders.cliente.Cliente;
import br.com.builderstest.builders.exception.ResourceNotFoundException;
import br.com.builderstest.builders.repository.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/v1")
public class Controller {
    @Autowired
    private ClienteRepository clienteRepository;

    @GetMapping("/clientes")
    public ResponseEntity<?> listAll(Pageable pageable){
        return new ResponseEntity<>(clienteRepository.findAll(pageable), HttpStatus.OK);
    }

    @GetMapping("/clientes/{id}")
    public ResponseEntity<Cliente> getClienteById(@PathVariable(value="id") Long id)
            throws ResourceNotFoundException {

           Cliente cliente = clienteRepository.findById(id)
                   .orElseThrow(()-> new ResourceNotFoundException("Client not found for this id :: " + id));
           return ResponseEntity.ok().body(cliente);
        }

    @GetMapping("/clientes/nome/{nome}")
    public ResponseEntity<Cliente> getClienteById(@PathVariable(value="nome") String nome) {
        Cliente cliente = clienteRepository.findBynome(nome);

                return ResponseEntity.ok().body(cliente);

    }

    @GetMapping("/clientes/cpf/{cpf}")
    public ResponseEntity<Cliente> getClienteByCpf(@PathVariable(value="cpf") String cpf)  {
        Cliente cliente = clienteRepository.findBycpf(cpf);
        return ResponseEntity.ok().body(cliente);

    }


    @PostMapping("/clientes")
    public Cliente createCliente(@Valid @RequestBody Cliente cliente){
        CalculoIdade calculoIdade = new CalculoIdade(cliente.getDataNascimento(),"dd/MM/yyyy");
       System.out.println("idade de : "+calculoIdade);
        return clienteRepository.save(cliente);
    }

    @PutMapping("/clientes/{id}")
    public ResponseEntity<Cliente> updateById(@PathVariable(value = "id") long id,@Valid @RequestBody Cliente clienteDetails) throws ResourceNotFoundException {
        Cliente cliente = clienteRepository.findById(id).orElseThrow(()-> new ResourceNotFoundException("Client not found for this id :: " + id));
        cliente.setNome(clienteDetails.getNome());
        cliente.setCpf(clienteDetails.getCpf());
        cliente.setDataNascimento(clienteDetails.getDataNascimento());
        final Cliente updateCliente = clienteRepository.save(cliente);
        return ResponseEntity.ok(updateCliente);
    }

    @PutMapping("/clientes/cpf/{cpf}")
    public ResponseEntity<Cliente> updateById(@PathVariable(value = "cpf") String cpf,@Valid @RequestBody Cliente clienteDetails)  {
        Cliente cliente = clienteRepository.findBycpf(cpf);
        cliente.setNome(clienteDetails.getNome());
        cliente.setCpf(clienteDetails.getCpf());
        cliente.setDataNascimento(clienteDetails.getDataNascimento());
        final Cliente updateCliente = clienteRepository.save(cliente);
        return ResponseEntity.ok(updateCliente);
    }

    @DeleteMapping("/clientes/{id}")
    public Map<String, Boolean> deleteClienteById(@PathVariable(value = "id") long id) throws ResourceNotFoundException {

            Cliente cliente = clienteRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Client not found for this id :: " + id));
            clienteRepository.deleteById(id);
            Map<String,Boolean> response = new HashMap<>();
            response.put("deleted", Boolean.TRUE);
            return response;
        }

    @DeleteMapping("/clientes/cpf/{cpf}")
    public Map<String, Boolean> deleteClienteById(@PathVariable(value = "cpf") String  cpf) {
        Cliente cliente = clienteRepository.findBycpf(cpf);
        clienteRepository.deleteBycpf(cpf);
        Map<String,Boolean> response = new HashMap<>();
        response.put("deleted", Boolean.TRUE);
        return response;
    }


    @PatchMapping("/clientes/cpf/{cpf}")
    public ResponseEntity<?> partialUpdateByCpf(
           @Valid @RequestBody Cliente clienteUpdate , @PathVariable("cpf") String cpf) {

        clienteRepository.save(clienteUpdate);
        return ResponseEntity.ok("resource address updated "+ clienteUpdate);
    }

    @PatchMapping("/clientes/{id}")
    public ResponseEntity<?> partialUpdateById(
            @Valid @RequestBody Cliente clienteUpdate , @PathVariable("id") long id) {

        clienteRepository.save(clienteUpdate);
        return ResponseEntity.ok("resource address updated "+ clienteUpdate );
    }

    }


