package br.com.builderstest.builders.repository;

import br.com.builderstest.builders.cliente.Cliente;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ClienteRepository extends JpaRepository<Cliente, Long> {

    public Cliente findBynome(String nome);

    public Cliente findBycpf(String cpf);

    public Cliente deleteBycpf(String cpf);
}
