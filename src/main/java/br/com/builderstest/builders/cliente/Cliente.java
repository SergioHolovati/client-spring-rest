package br.com.builderstest.builders.cliente;


import javax.persistence.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Period;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

@Entity
public class Cliente {

    private long id;
    private String nome;
    private String cpf;
    private String dataNascimento;
    private int idade;

    public Cliente(){
    super() ;
    }

    public Cliente(String nome, String cpf, String dataNascimento){
        this.nome = nome;
        this.cpf = cpf;
        this.dataNascimento = dataNascimento;
        this.idade = idade;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public long getId() {
        return id;
    }


    public void setId(long id) {
        this.id = id;
    }

    @Column(nullable = false)
    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    @Column(nullable = false)
    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }
    @Column(nullable = false)
    public String getDataNascimento() {
        return dataNascimento;
    }


    public void setDataNascimento(String dataNascimento) {
        this.dataNascimento = dataNascimento;
    }

    public Integer getIdade() throws ParseException {
        Date dateNasc =new SimpleDateFormat("dd/MM/yyyy").parse(this.dataNascimento);

        GregorianCalendar hj=new GregorianCalendar();
        GregorianCalendar nascimento=new GregorianCalendar();
        if(this.dataNascimento !=null){
            nascimento.setTime(dateNasc);
        }
        int anohj=hj.get(Calendar.YEAR);
        int anoNascimento=nascimento.get(Calendar.YEAR);
        idade = anohj-anoNascimento;
        return idade;
    }

    public void setIdade(int idade) {
                
        this.idade = idade;
    }

    @Override
    public String toString(){
        return "Clientes [id : "+id +", Nome: "+nome+", CPF: "+ cpf+", Nascimento: "+dataNascimento+", Idade : "+idade+"]";
    }


}
