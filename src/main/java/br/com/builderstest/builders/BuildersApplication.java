package br.com.builderstest.builders;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories
public class BuildersApplication {

    public static void main(String[] args) {
        SpringApplication.run(BuildersApplication.class, args);
    }

}
